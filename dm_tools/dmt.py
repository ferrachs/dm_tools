#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Sylvaine Ferrachat 2018-11
Tool to deploy a directory hierarchy for describing and archiving model
experiment data, paper...
"""
 
#------------------------------------------
#-- Import preamble:
import datetime as dt
import fnmatch
import json
import os
import re
from pathlib import Path
from pkg_resources import resource_filename
import subprocess
import sys

import begin
import sh
#>>debug
from pprint import pprint
#<<debug

#------------------------------------------
#-- Defaults and constants

# Runtime defaults
DEFT_rootdir = os.getcwd()
DEFT_date_started = 'YYYY-MM'
DEFT_action = 'create'
DEFT_userconfig = os.path.join(os.path.expanduser('~'),'.DM_toolsconfig')

# DM tool file organization
datapath = resource_filename(__name__,'data')

# Define 'properties' that will be used to automatically update specific places in the editable files (mostly editables).
with open(os.path.join(datapath,'properties.json')) as pf:
    DM_props = json.load(pf)

# Get blob_cat and their aliases
blob_cat = {}
for key in DM_props.keys():
    if key == 'DM_global':
        pass
    else:
        blob_cat[key.strip('DM_')] = DM_props[key]['aliases']

# editable file patterns
editable_patt = ('README.*','associated_experiments.txt')

# Some date management
today = dt.datetime.today()
today_ymd = today.strftime('%Y-%m-%d')
today_ym = today.strftime('%Y-%m')

# Version management
DM_tools_version_text = 'DM_tools version: ' + DM_props['DM_global']['DM_tools_version']

#------------------------------------------
#-- Utility functions

def underline(s:str):
    if s[-1] == '\n':
        s = s[:-1]
        suff = '\n'
    else:
        suff = ''
    return s+'\n'+'-'*len(s)+ suff

def translate_bc(s:str):
    """
    Translate s into an existing blob category name
    """

    if s not in [k for k in blob_cat.keys()] + \
            [blob_cat[k][i] for k in blob_cat.keys() for i in range(len(blob_cat[k]))]:
        print('Error! Unknown blob category! See global option "--list-cats" for details')
        sys.exit(1)

    for key in blob_cat.keys():
        if s in blob_cat[key] or s == key:
            return key

def set_context(**kwargs):
    """
    Define custom begin.context attributes for availability of data inside subcommands
    """

    for key,val in kwargs.items():
        begin.context.__setattr__(key,val)

def replace_kwd_by_value(*iofile:str,global_props:dict=None,cat_props:dict=None):
    """
    Takes one or several txt files, parse every occurence of given keywords,
    and replace them by their corresponding value
    global_props are simply replaced everywhere in parsed text
    category_props are only replaced in lines starting with:  'Some prop:' and will replace
    the entire string after 'Some prop:'
    This function also inserts a line to document the current version of the DM_tools
    """

    #-- Pre-compile re patterns
    patt = []
    for k in cat_props.keys():
        patt.append(re.compile(r'^('+k+':).*$'))

    #-- Replace matching patterns
    for file in iofile:
        fh = open(file,'r')
        olines = fh.readlines()
        fh.close()
    
        nlines = []
        for l in olines:
            for k,v in global_props.items():
                l = l.replace(k,str(v))
            for p,v in zip(patt,cat_props.values()):
                string = r'\1 '+str(v)+r'\n'
                m = p.match(l)
                if m:
                    l = m.group(1)+' ' + str(v)+u'\n'
            nlines.append(l)
    
        #-- Prepend a line documenting the current DM_tools version
        nlines = [u'# ' + DM_tools_version_text + u'\n', *nlines]

        fh = open(file,'w')
        fh.writelines(nlines)
        fh.close()

def prop_arg_type(string:str): 
    """
    Defines a complex type for command option 'prop'
    'string' should be of the form:
    'key1=val1;key2=val2;...'
    Spaces are tolerated.
    Warning! Will silently dismiss any mis-formatted key,val pair
    """
    prop_list =  [tuple(y.strip() for y in x.split('=')) for x in string.split(';')]
    clean_prop_list = [x for x in prop_list if len(x)==2]
    return dict(clean_prop_list)

def validate_props(props:dict,user_def_props:dict):
    """
    Check if user-defined properties are existing
    If not, drop the faulty element
    If yes, update props
    """

    for k,v in user_def_props.items():
        if k in props.keys():
            props[k] = v
        else:
            print('Warning! Property {} does not exist!'.format(k))
            print('Dropping it!')
    return props

def user_config():
    """
    Create or read user config
    and update global properties subsequently
    """

    # ToDo this is quick and dirty for now
    config_file = Path(DEFT_userconfig)
    if not config_file.is_file():
        print('You seem to not have any DM_tools config file. Creating one...')
        config_props = {}
        config_props['DM_tools_version'] = DM_props['DM_global']['DM_tools_version']
        config_props['LastName'] = input(u'Your last name?\n')
        config_props['FirstName'] = input(u'Your first name?\n')

        fh = open(DEFT_userconfig,'w')
        json.dump(dict(DM_global=config_props),fh, 
                indent = 4,
                separators=(',', ': ')
                )

    else:
        all_props = json.load(open(DEFT_userconfig))
        config_props = all_props['DM_global']

    update_global_props(config_props)


def update_global_props(config_props:dict):
    # the whole function is a draft and very brittle
    # ToDo to refactor!!

    for k,v in config_props.items():
        DM_props['DM_global'][k] = str(v)

    # Todo hardcoding to refactor?
    DM_props['DM_global']['YYYY-MM'] = today_ym

#------------------------------------------
#-- Classes
class Blob_category:

    """
    Data structure to handle a fundamentals of an archiving blob
    """

    def __init__(self,category):
        category.lower()
        self.category = category
        self.abbrv = self.category[0:3]
        self.templ = os.path.join(datapath,
                'Templates',self.category.capitalize(),'') # append path separator
        # ToDo
        #self.kwd_list

class Blob(Blob_category):

    """
    Data structure to handle an archiving blob (experiment, paper,...)
    """

    def __init__(self,category, name, basedir, user_def_props, **kwargs):
        Blob_category.__init__(self,category)
        self.name = name
        self.basedir = basedir
        self.set_props(user_def_props)
        self.set_id()
        self.dirname = self.id # ToDo this may change in future

        #-- Handling of optional kwargs
        valid_optkwargs = (
                'overw',
                )
        for k in valid_optkwargs:
            self.__setattr__(k,None)

        for k in kwargs.keys():
            if k in valid_optkwargs:
                self.__setattr__(k,kwargs[k])

    def set_props(self,user_def_props):
        self.global_props = {}
        self.global_props.update(DM_props['DM_global'])

        self.cat_props = {}
        self.cat_props.update(DM_props['DM_'+self.category])

        #-- Update props from user-given props
        if user_def_props:
            self.cat_props = validate_props(self.cat_props,user_def_props)

        #-- Special handling for object name if relevant:
        name_key = self.category.capitalize() + ' name'
        if name_key in self.cat_props.keys():
            self.cat_props[name_key] = self.name

        # ToDo
        #-- Special treatment for object ID:
        pass


    def set_id(self):
        #-- ToDo
        # This is simply a skeleton for now
        if self.category == 'experiment':
            self.id = self.name
        elif self.category == 'paper':
            self.id = self.name

    def update_editable_files(self,dirname):
        editable_files = []
        for rootdir, dirs, files in os.walk(dirname):
            for file in files:
                matches = [fnmatch.fnmatch(file,p) for p in editable_patt]
                if any(matches):
                    editable_files.append(os.path.join(rootdir, file))

        replace_kwd_by_value(*editable_files,
                global_props = self.global_props,
                cat_props = self.cat_props,
                )

        return editable_files


    def create(self):
        #-- Create directory (with parents if necessary)
        category_dirname = self.category.capitalize() + 's'
        if Path(self.basedir).name == category_dirname:
            middle_path = ''
        else:
            middle_path = category_dirname

        directory = Path(self.basedir,middle_path,self.dirname)
        fulldirname = directory.__str__()

        try:
            directory.mkdir(parents=True)
        except FileExistsError:
            if self.overw != True:
                print('Error! {} already exists! Use option --overw to force overwriting.'.format(fulldirname))
                sys.exit(1)
            else:
                print('Warning! Overwriting {}'.format(fulldirname))

        #-- Copy from template
        output = sh.rsync('-a','--exclude','.gitkeep',self.templ,fulldirname, _encoding='UTF-8')
        #Todo: handle logging properly
        #print(output.stdout.decode())

        #-- Update the editable files
        edited_files = self.update_editable_files(fulldirname)

        #-- Finalize
        try:
            reldirname = os.path.join('.',directory.relative_to(Path.cwd()).__str__())
        except ValueError:
            reldirname = fulldirname

        msg = '\nSuccessfully created a new archive blob in {}!\n\n'.format(reldirname) + \
            underline('Don\'t forget to:\n') + \
            ' '*4 +'* Review and update the following files:\n' + \
            ''.join([' '*8+'- '+Path(s).relative_to(fulldirname).__str__()+'\n' for s in edited_files]) + \
            ' '*4 + '* Fill the directories with relevant files' + \
            '\n'

        print(msg)
        return

    def update(self):
        # ToDo
        print('Not yet implemented! Doing nothing')
        pass

        return
        
    def rename(self,new_name):
        # ToDo
        print('Not yet implemented! Doing nothing')
        pass

        return

#------------------------------------------
#-- Subcommands

@begin.subcommand
@begin.convert(props=prop_arg_type)
def new(
        blob_category : 'Archive blob category (see global option "--list-cats" for a list of categories)',
        blob_name : 'Archive blob name',
        props : 'Set properties (see global option "--list-props") in the form "key1=val1; key2=val2;..."' = None,
        overw : 'Force overwriting of an existing archive blob' = False,
        ):

    """
    Create a new archive hierarchy (aka archive 'blob').
    """

    blob = Blob(translate_bc(blob_category.lower()),
            blob_name,
            basedir = begin.context.basedir,
            user_def_props = props,
            overw = overw,
            )

    e = blob.create()

@begin.subcommand
@begin.convert(props=prop_arg_type)
def upd(
        blob_category : 'Archive blob category (see global option "--list-cats" for a list of categories)',
        blob_name : 'Archive blob name',
        props : 'Set properties (see global option "--list-props") in the form "key1=val1; key2=val2;..."' = None,
        ):

    """
    Update an existing archive hierarchy (aka archive 'blob'). !! Not yet implemented!!
    """

    blob = Blob(translate_bc(blob_category.lower()),
            blob_name,
            basedir = begin.context.basedir,
            user_def_props = props,
            )

    e = blob.update()

@begin.subcommand
def mv(
        blob_category : 'Archive blob category (see global option "--list-cats" for a list of categories)',
        old_blob_name : 'Current archive blob name',
        new_blob_name : 'New archive blob name',
        ):

    """
    Rename an existing archive hierarchy (aka archive 'blob'). !! Not yet implemented!!
    """

    blob = Blob(translate_bc(blob_category.lower()),
            old_blob_name,
            basedir = begin.context.basedir,
            user_def_props = None,
            )

    e = blob.rename(new_blob_name)

#------------------------------------------
#-- Main

@begin.start(cmd_delim='--')
def run(
        rootdir : 'Root dir to create archive(s)' = DEFT_rootdir,
        list_props : 'Do nothing but printing the list of available properties' = False,
        list_cats : 'Do nothing but printing the list of available archive blob categories and their aliases' = False,
        version : 'Print DM_tools version and exit' = False,
        ):

    """
    Tool to deploy a directory hierarchy for describing and archiving model experiment data, paper...
    """

    #-- Set base directory
    basedir = os.path.expanduser(rootdir)

    #-- Set context for subcommands
    set_context(basedir=basedir)

    #-- List available properties if relevant
    if list_props == True:
        print()
        print(underline('Available properties:'))
        indent0 = ' '*4
        indent1 = indent0*2

        for k0,v0 in DM_props.items():
            print(indent0,k0.strip('DM_')+':')
            for k1,v1 in v0.items():
                print(indent1,'"'+k1+'"',':',v1)
            print()
        sys.exit(0)

    #-- List available blobs if relevant
    if list_cats == True:
        print()
        print(underline('Available archive blob categories (name : list of aliases):'))
        indent0 = ' '*4

        for key in blob_cat.keys():
            print(indent0,key,': ',blob_cat[key])
        print()
        sys.exit(0)

    #-- Print version if relevant
    if version == True:
        print(DM_tools_version_text)
        sys.exit(0)

    #-- Handle user config
    user_config()
