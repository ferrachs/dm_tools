#--------------------------------------------------
#
# FirstName LastName, YYYY-MM
#
#--------------------------------------------------

---------------------------
A. Basic paper description:
---------------------------

Authors:
Contact person:
Title:
Journal:
doi:
Date published:

----------------------
B. Directory contents:
----------------------

    * associated_experiments.txt <--- contains minimalistic info for all experiments performed / used for the paper

    * Other_data/ <--- all but experiment data, e.g. tabulated data (csv, excel) created for the paper, observations, data from other papers used for comparison, etc. [don't forget to fill in the README there]

    * Plots/ <--- the paper plots [and their script if relevant, or a small README file describing the SW used...]

    * Paper_material/ <--- material used for writing the paper [e.g. .tex, .docx...]
