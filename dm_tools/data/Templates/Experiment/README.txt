#--------------------------------------------------
#
# FirstName LastName, YYYY-MM
#
#--------------------------------------------------

---------------
A. Description:
---------------

Author:
Contact person:

Experiment name:

Source code name:
Source code tag:

Horizontal resolution:
Vertical resolution:
Model start date:
Model stop date:
Is a restart from experiment:
Input files version:
Used non-standard input:

Machine used:
Compiler name and version:
Experiment started on:

Associated paper reference:
Associated paper storage location: 

Scientific summary:


[Basic scientific description: context, most prominent aspects of the setup, etc.]


------------
B. Contents:
------------

    * Experiment_definition/ <--- all the files that describe the experiment [settings_EXP, emi_spec_EXP.txt, symlinks_EXP.sh, source_info_EXP.txt, patch_EXP.diff, log_EXP_TIMESTAMP.txt -maybe several-, config.log, batch job output files...]

            [OPTIONAL: * Input_ data/ <--- input files information [store there a README.txt that documents the input file distribution version used + all the non-standard files, ie not pertaining to the official input file distribution for this model.] Drop this item if only standard input files where used]

    * Raw_data/ <--- as it says, (some of) the raw data of the experiment [regular output + restart files. No need to keep the links to the input files. You may or may not keep the program log, given that it might actually already be fully contained in the job output files]

    * Pproc_data/ <--- as it says, post-processed data from the raw data [again, a README.txt describing what kind of p-proc was performed is good. If p-proc is very specific, providing the script or a link to the repo location of the script is good too. If the p-proc data is uploaded to a public repo, please provide also the associated doi]

    [OPTIONAL: * Plots/ <--- associated plots [if relevant. This may be fully covered by in the associated paper storage] ]
