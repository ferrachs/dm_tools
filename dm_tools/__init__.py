#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Sylvaine Ferrachat 2018-11
Tool to deploy a directory hierarchy for describing and archiving model
experiment data, paper... It provides facilities to auto-fill readme files with general and user-defined properties / metadata.
"""

from .dmt import Blob
