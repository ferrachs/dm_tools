#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
        name="dm_tools",
        version="0.1.0",
        packages=find_packages(),
        include_package_data=True,
        entry_points={
            'console_scripts': [
                'dmt = dm_tools.dmt:run.start'
                ]
            },
        install_requires=['begins>=0.9','sh>=1.12'],
        )
